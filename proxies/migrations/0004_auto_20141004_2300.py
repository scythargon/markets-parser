# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('proxies', '0003_proxy_just_added'),
    ]

    operations = [
        migrations.AddField(
            model_name='proxy',
            name='active_android',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='proxy',
            name='active_iphone',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='proxy',
            name='active_windows',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='proxy',
            name='last_use_android',
            field=models.DateTimeField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='proxy',
            name='last_use_iphone',
            field=models.DateTimeField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='proxy',
            name='last_use_windows',
            field=models.DateTimeField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
