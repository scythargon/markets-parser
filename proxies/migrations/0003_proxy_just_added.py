# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('proxies', '0002_auto_20140929_0943'),
    ]

    operations = [
        migrations.AddField(
            model_name='proxy',
            name='just_added',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
    ]
