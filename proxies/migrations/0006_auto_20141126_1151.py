# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('proxies', '0005_auto_20141005_1546'),
    ]

    operations = [
        migrations.AlterField(
            model_name='proxy',
            name='active',
            field=models.BooleanField(default=True),
        ),
    ]
