# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Proxy',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ip', models.CharField(max_length=225)),
                ('port', models.IntegerField(null=True, blank=True)),
                ('type', models.CharField(max_length=225, null=True, blank=True)),
                ('added', models.DateTimeField(auto_now_add=True, null=True)),
                ('anonymity', models.CharField(max_length=225, null=True, blank=True)),
                ('active', models.BooleanField(default=True)),
                ('last_try', models.DateTimeField(null=True, blank=True)),
                ('last_success', models.DateTimeField(null=True, blank=True)),
                ('successful_requests', models.IntegerField(default=0)),
                ('failed_requests', models.IntegerField(default=0)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
