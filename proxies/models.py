import logging
import threading
import time

from django.db import models
from datetime import datetime


class Proxy(models.Model):
    ip = models.CharField(max_length=225)
    port = models.IntegerField(blank=True, null=True)
    type = models.CharField(max_length=225, blank=True, null=True)
    added = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    anonymity = models.CharField(max_length=225, blank=True, null=True)

    just_added = models.BooleanField(default=True)
    active = models.BooleanField(default=True)
    last_try = models.DateTimeField(blank=True, null=True)
    last_success = models.DateTimeField(blank=True, null=True)
    successful_requests = models.IntegerField(default=0)
    failed_requests = models.IntegerField(default=0)

    active_windows = models.BooleanField(default=True)
    active_android = models.BooleanField(default=True)
    active_iphone = models.BooleanField(default=True)

    last_use_windows = models.DateTimeField(default=datetime(1970, 1, 1))
    last_use_android = models.DateTimeField(default=datetime(1970, 1, 1))
    last_use_iphone = models.DateTimeField(default=datetime(1970, 1, 1))


    def use_by_platform(self, platform):
        setattr(self, 'last_use_%s' % platform.lower(), datetime.now())
        self.save()

    def deactivate(self):
        self.active = False
        self.last_try = datetime.now()
        self.save()

    def deactivate_for_platform(self, platform):
        setattr(self, 'last_use_%s' % platform.lower(), datetime.now())
        setattr(self, 'active_%s' % platform.lower(), False)
        self.save()

    @classmethod
    def get_just_checked_one(cls):
        from .management.commands.check_proxies import ProxyChecker

        for proxy in Proxy.objects.all():
            ProxyChecker.check_proxy(proxy)
            if proxy.active:
                return proxy

    def __unicode__(self):
        return '%s:%s' % (self.ip, self.port)


    @staticmethod
    def quick_give_me_working_one__(threads_amount=2, platform=None):
        from .management.commands.check_proxies import ProxyChecker
        ProxyChecker.init(just_one=True, last_used_by=platform)

        threads = []
        for i in range(threads_amount):
            t = threading.Thread(target=ProxyChecker.worker)
            threads.append(t)
            t.start()
        for t in threads:
            t.join()
        return ProxyChecker.first_found


    @staticmethod
    def quick_give_me_working_one(platform):
        while True:
            p = Proxy.objects.filter(**{'active_%s' % platform.lower() : True, 'active': True}).order_by('last_use_%s' % platform.lower())
            if p.count():
                return p[0]
            else:
                logging.info('waiting 10s for active proxy for platform %s' % platform)
                time.sleep(10)
