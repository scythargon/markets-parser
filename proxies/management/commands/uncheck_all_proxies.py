import requests
import re
import ipdb
import Queue, threading

from bs4 import BeautifulSoup
import datetime

from django.core.management.base import BaseCommand, CommandError
from proxies.models import Proxy

from app_metrics.utils import create_metric, gauge
from app_metrics.models import Metric


import warnings
warnings.filterwarnings("ignore", category=RuntimeWarning)



def uncheck_all_proxies(threads_amount=20):

    Proxy.objects.all().update(active=False)


class Command(BaseCommand):
    args = 'bla bla bla'

    def handle(self, *args, **options):

        uncheck_all_proxies()