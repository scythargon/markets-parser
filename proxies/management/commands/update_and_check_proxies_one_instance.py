import requests
import re
import ipdb
import os, sys
from bs4 import BeautifulSoup
import datetime

from django.core.management.base import BaseCommand, CommandError
from proxies.models import Proxy

from app_metrics.utils import create_metric, gauge
from app_metrics.models import Metric

from .update_proxies import update_proxies
from .check_proxies import check_proxies



class Command(BaseCommand):
    args = 'bla bla bla'

    def handle(self, *args, **options):

        pid = str(os.getpid())
        pidfile = "/tmp/update_and_check_proxies_one_instance.pid"

        if os.path.isfile(pidfile):
            print "%s already exists, exiting" % pidfile
            sys.exit()
        else:
            file(pidfile, 'w').write(pid)

        update_proxies()
        check_proxies()

        os.unlink(pidfile)