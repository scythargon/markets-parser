import requests
from bs4 import BeautifulSoup

from django.core.management.base import BaseCommand, CommandError
from proxies.models import Proxy

from app_metrics.utils import create_metric, gauge
from app_metrics.models import Metric


def update_proxies():
    r = requests.get('http://free-proxy-list.net/')
    bs = BeautifulSoup(r.text, 'lxml')

    before = Proxy.objects.count()
    added = 0

    for tr in bs.find('table', {'id':'proxylisttable'}).findAll('tr')[1:-1]:
        ip, port, country_code, country_name, anonymity, google, https, last_checked = [td.text for td in tr.findAll('td')]
        if anonymity not in ['anonymous', 'elite proxy']:
            continue
        p, created = Proxy.objects.get_or_create(ip=ip, port=port, anonymity=anonymity)
        if created:
            added += 1
            print 'new proxy is added: %s, total: %s' % (added, before + added)
    after = Proxy.objects.count()
    gauge('total-proxies', after)
    print 'before: %s, after: %s' % (before, after)


class Command(BaseCommand):
    args = 'bla bla bla'

    def handle(self, *args, **options):
        update_proxies()