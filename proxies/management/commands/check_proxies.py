import requests
import re
import ipdb
import Queue, threading

from bs4 import BeautifulSoup
import datetime

from django.core.management.base import BaseCommand, CommandError
from proxies.models import Proxy

from app_metrics.utils import gauge
from app_metrics.models import Metric

import warnings
warnings.filterwarnings("ignore", category=RuntimeWarning)



class ProxyChecker():

    @classmethod
    def init(cls,just_one=False, last_used_by=None):
        cls.q = Queue.Queue()
        proxies = []
        Proxy.objects.filter(active=False).delete()
        if last_used_by:
            proxies = Proxy.objects.filter(**{'active_%s' % last_used_by.lower() : True}).order_by('last_use_%s' % last_used_by.lower())
        else:
            proxies = Proxy.objects.all()
        for proxy in proxies:
            cls.q.put(proxy)

        cls.just_one = just_one
        cls.first_found = False

        cls.init_values()


    @classmethod
    def init_values(cls):
        cls.total = Proxy.objects.count()
        cls.failed = 0
        cls.succeeded = 0
        cls.timed_out = 0
        cls.passed = 0
        cls.enabled = 0
        cls.disabled = 0
        r = requests.get('http://whatismyip.ru/')
        cls.original_ip = re.findall(r'\d+\.\d+\.\d+\.\d+', r.text)[0]

    @classmethod
    def worker(cls):
        while (not cls.q.empty()) and ((not cls.just_one) or (cls.just_one and not cls.first_found)):
            try:
                proxy = cls.q.get(False)
                cls.check_proxy(proxy)
            except Queue.Empty:
                pass

    @classmethod
    def check_proxy(cls, proxy):
        if not hasattr(cls, 'original_ip'):
            cls.init_values()

        ip = cls.original_ip
        try:
            r = requests.get('http://whatismyip.ru/', proxies={'http': 'http://%s:%s' % (proxy.ip, proxy.port)}, timeout=5)
        except (requests.exceptions.ConnectTimeout, requests.exceptions.ReadTimeout, requests.exceptions.ConnectionError):
            cls.timed_out += 1
            proxy.active = False
            proxy.last_try = datetime.datetime.now()
        else:
            ip = re.findall(r'\d+\.\d+\.\d+\.\d+', r.text)
            if len(ip) == 0:
                proxy.active = False
                proxy.last_try = datetime.datetime.now()
                cls.disabled += 1
                cls.failed += 1
            else:
                ip = ip[0]
            if ip == cls.original_ip:
                proxy.active = False
                proxy.last_try = datetime.datetime.now()
                cls.disabled += 1
                cls.failed += 1
            else:
                if proxy.active == False and proxy.just_added == False:
                    cls.enabled += 1
                proxy.active = True
                cls.first_found = proxy
                proxy.last_try = proxy.last_success = datetime.datetime.now()
                cls.succeeded += 1
        proxy.just_added = False
        proxy.save()
        cls.passed += 1
        print '%s/%s succeeded:%s failed:%s timed_out:%s enabled: %s %s' % (cls.passed, cls.total, cls.succeeded, cls.failed, cls.timed_out, cls.enabled, '' if ip == cls.original_ip else ip)


def check_proxies(threads_amount=70):

    before = Proxy.objects.filter(active=True).count()

    ProxyChecker.init()
    threads = []
    for i in range(threads_amount):
        t = threading.Thread(target=ProxyChecker.worker)
        threads.append(t)
        t.start()
    for t in threads:
        t.join()

    after = Proxy.objects.filter(active=True).count()
    gauge('active-proxies', after)
    gauge('proxies-enabled', ProxyChecker.enabled)
    gauge('proxies-disabled', ProxyChecker.disabled)
    print 'before: %s, after: %s' % (before, after)



class Command(BaseCommand):
    args = 'bla bla bla'

    def handle(self, *args, **options):

        check_proxies()