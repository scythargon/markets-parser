"""
Django settings for markets_parser project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import warnings
warnings.filterwarnings("ignore", category=RuntimeWarning)


import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

import djcelery
djcelery.setup_loader()


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '%8#a^ff1w9)t$+-o6mj1+_%0u31_cd7_q63s-&%%ka^t55k^6u'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []

RUNNING_ADDRESS = '127.0.0.1:8000'

# Application definition

INSTALLED_APPS = (
    'suit',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'markets_parser',
    'proxies',
    'suit_redactor',
    'app_metrics',
    'djcelery',
)


MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)


BROKER_URL = 'amqp://guest:guest@localhost:5672//'

APP_METRICS_LIBRATO_USER = 'scythargon@gmail.com'
APP_METRICS_LIBRATO_TOKEN = '53001862c15c2582f79c3bd72d84d4d4a6c60183cdbac40e39eeb67e6844edcd'  # yeah lets just upload all this shit to public repo.
APP_METRICS_LIBRATO_SOURCE = 'dev'

APP_METRICS_MIXPANEL_TOKEN = 'ab724050b97d79e88ea3a3a66f78a19c'

APP_METRICS_BACKEND = 'app_metrics.backends.composite'
APP_METRICS_COMPOSITE_BACKENDS = ('app_metrics.backends.librato', 'app_metrics.backends.mixpanel') # ,


ROOT_URLCONF = 'markets_parser.urls'

WSGI_APPLICATION = 'markets_parser.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'markets_parser',                      # Or path to database file if using sqlite3.
        'USER': 'argon',
        'PASSWORD': '',
        'HOST': 'localhost',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '6432'
    }
}

with open('server_id.txt') as f:
    server_id = f.read().strip()
    if server_id == 'walrus':
        DATABASES['default']['USER'] = 'postgres'
        RUNNING_ADDRESS = '159.253.18.235:8666'

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/


import os
BASE_DIR = '/'.join(os.path.dirname(os.path.abspath(__file__)).split('/')[:-1]) + '/'

#STATIC_ROOT = BASE_DIR + 'static/'
STATIC_URL = '/static/'
SERVE_STATICS = True

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "static"),
)

from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS as TCP

TEMPLATE_CONTEXT_PROCESSORS = TCP + (
    'django.core.context_processors.request',
    )

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'colored': {
            'format' : bcolors.OKBLUE + "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s" + bcolors.ENDC,
            'datefmt' : "%d/%b/%Y %H:%M:%S"
        },
    },
    'handlers': {
        'file-colored': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': BASE_DIR + 'debug.log',
            'formatter': 'colored'
        },
        'errors': {
            'level': 'ERROR',
            'class': 'logging.FileHandler',
            'filename': BASE_DIR + 'errors.log',
            'formatter': 'colored'
        },
    },
    'loggers': {
        '': {
            'handlers': ['file-colored', 'errors'],
            'level': 'DEBUG',
            'propagate': True,
        },
    },
}

'''
all the tasks:
  . app_metrics.tasks.db_gauge_task
  . app_metrics.tasks.db_metric_task
  . app_metrics.tasks.librato_metric_task
  . app_metrics.tasks.mixpanel_metric_task
  . app_metrics.tasks.redis_gauge_task
  . app_metrics.tasks.redis_metric_task
  . app_metrics.tasks.statsd_gauge_task
  . app_metrics.tasks.statsd_metric_task
  . app_metrics.tasks.statsd_timing_task
  . markets_parser.tasks.games_parsed_report
  . markets_parser.tasks.parse_url
'''

CELERYD_CONCURRENCY = 5
CELERYD_MAX_TASKS_PER_CHILD = 2
CELERYD_PREFETCH_MULTIPLIER = 1

CELERY_DEFAULT_QUEUE = 'default'
CELERY_DEFAULT_EXCHANGE_TYPE = 'default'
CELERY_DEFAULT_ROUTING_KEY = 'default'

CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'

from kombu import Exchange, Queue

CELERY_QUEUES = (
    Queue('default', Exchange('default'), routing_key='default'),
    Queue('parsers', Exchange('parsers'), routing_key='long_tasks'),
)
CELERY_ROUTES = {
    'markets_parser.tasks.parse_url': {
        'queue': 'parsers',
        'routing_key': 'long_tasks',
    },
}


from datetime import timedelta

CELERYBEAT_SCHEDULE = {
    'games-parsed-report': {
        'task': 'markets_parser.tasks.games_parsed_report',
        'schedule': timedelta(minutes=1),
    },
    'tasks-in-queue': {
        'task': 'markets_parser.tasks.tasks_in_queue',
        'schedule': timedelta(minutes=5),
    },
}


import logging

logging.getLogger("requests").setLevel(logging.WARNING)