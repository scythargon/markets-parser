from django.contrib import admin
from django import forms
from django.forms import ModelForm
from django.db import models
from suit_redactor.widgets import RedactorWidget
from .models import *
from .widgets import *

class AndroidGameInline(admin.TabularInline):
    model = AndroidGame
    suit_classes = 'suit-tab suit-tab-android'
    verbose_name_plural = 'Android Version'


class IphoneGameInline(admin.TabularInline):
    model = IPhoneGame
    suit_classes = 'suit-tab suit-tab-iphone'
    verbose_name_plural = 'iPhone Version'


class WindowsGameInline(admin.TabularInline):
    model = WindowsGame
    suit_classes = 'suit-tab suit-tab-windows'
    verbose_name_plural = 'Windows Version'


class GameForm(ModelForm):
    class Meta:
        widgets = {
            'description': RedactorWidget(editor_options={'lang': 'ru'}),
            'whats_new': RedactorWidget(editor_options={'lang': 'ru'}),
            'screenshots': ScreenshotsThumbnailsWidget({}),
            'ipad_screenshots': ScreenshotsThumbnailsWidget({}),
            'icon': IconThumbnailWidget({}),
            'video': VideoWidget({}),
        }

    def clean(self, *args, **kwargs):
        super(GameForm, self).clean(*args, **kwargs)

        for f in self.instance._meta.get_all_field_names():
            if f in self.cleaned_data:
                field = self.instance._meta.get_field_by_name(f)[0]
                if isinstance(field, models.ManyToManyField):
                    import ipdb
                    ipdb.set_trace()
                    setattr(self.instance,f,self.cleaned_data[f])


class AndroidGameAdmin(admin.ModelAdmin):
    model = AndroidGame
    form = GameForm


class IPhoneGameAdmin(admin.ModelAdmin):
    model = IPhoneGame
    form = GameForm


class WindowsGameAdmin(admin.ModelAdmin):
    model = WindowsGame
    form = GameForm


class GameAdmin(admin.ModelAdmin):
    inlines = [
        AndroidGameInline, IphoneGameInline, WindowsGameInline
    ]
    model = Game
    suit_form_tabs = (('android', 'Android'), ('iphone', 'iPhone'), ('windows', 'Windows'))


#admin.site.register(AndroidGame, AndroidGameInline)
#admin.site.register(IphoneGame, IphoneGameInline)
#admin.site.register(WindowsGame, WindowsGameInline)

#admin.site.register(Game, GameAdmin)
admin.site.register(AndroidGame, AndroidGameAdmin)
admin.site.register(IPhoneGame, IPhoneGameAdmin)
admin.site.register(WindowsGame, WindowsGameAdmin)
'''
for model in [Publisher, Category, PricingType, Language, AgeCategory, UsersAmount, Screenshot, Video, Currency]:
    admin.site.register(model)
'''