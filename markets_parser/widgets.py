from django.forms import widgets
from django.utils.safestring import mark_safe
from django.template.loader import render_to_string



class ScreenshotsThumbnailsWidget(widgets.Textarea):

    template_name = 'screenshots_thumbnails_widget.html'

    class Media:
        js = (
            'js/jquery.colorbox-min.js',
            'js/base.js',
        )
        css = {
            'all': (
                'css/colorbox.css',
                'css/base.css',
            )
        }

    def render(self, name, value, attrs=None):
        urls = []
        if value:
            urls = value.split()
        '''
        for s in self.choices.queryset:
            urls.append(s.img_url)
        '''
        return mark_safe(render_to_string(self.template_name, locals()))


class IconThumbnailWidget(widgets.Textarea):

    template_name = 'screenshots_thumbnails_widget.html'

    def render(self, name, value, attrs=None):
        if value:
            urls = [value]
        return mark_safe(render_to_string(self.template_name, locals()))


class VideoWidget(widgets.Textarea):

    template_name = 'video_widget.html'

    def render(self, name, value, attrs=None):
        return mark_safe(render_to_string(self.template_name, locals()))
