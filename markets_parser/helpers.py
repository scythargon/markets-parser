import socket
import json


def recvall(sock):
    data = ""
    part = None
    while part != "":
        part = sock.recv(4096)
        data+=part
    return data


def download_via_socket(url, proxy=None):
    sock = socket.socket()
    sock.connect(('localhost', 8002))
    request = {'url': url}
    if proxy:
        p = {'ip': proxy.ip, 'port':proxy.port}
        request['proxy'] = p
    request = json.dumps(request)
    if len(request) > 4050:
        raise Exception("Too long request, it's time to implement message boundaries!")
    sock.send(request)
    data = recvall(sock)
    sock.close()
    return data


def log_to_file(name, data):
    filename = '/tmp/log_%s' % name
    f = open(filename, 'w')
    f.write(data)
    f.close()
    print '==== logged to %s' % filename