"""
WSGI config for markets_parser project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/howto/deployment/wsgi/
"""

import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "markets_parser.settings")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
