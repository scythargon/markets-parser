from celery import task
from app_metrics.utils import gauge

@task
def parse_url(url, platform, page_type, proxy_pk=None, **kwargs):
    from markets_parser.traversers import WindowsTraverser, IPhonesTraverser, AndroidTraverser

    cls = None
    classes = (WindowsTraverser, IPhonesTraverser, AndroidTraverser)
    for c in classes:
        if c.PLATFORM_ID == platform:
            cls = c

    cls.traverse(page_type, url, proxy_pk, **kwargs)


@task
def games_parsed_report():
    from markets_parser.models import WindowsGame, IPhoneGame, AndroidGame
    gauge('windows-games-parsed', WindowsGame.objects.count())
    gauge('iphone-games-parsed', IPhoneGame.objects.count())
    gauge('android-games-parsed', AndroidGame.objects.count())


@task
def tasks_in_queue():
    import os
    data = os.popen("rabbitmqctl list_queues").read()
    data = data.split()
    if 'parsers' in data:
        parsers_tasks = int(data[data.index('parsers') + 1])
        gauge('parser-tasks-in-queue', parsers_tasks)
