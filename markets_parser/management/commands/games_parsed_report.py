from django.core.management.base import BaseCommand

from markets_parser.tasks import games_parsed_report

import warnings
warnings.filterwarnings("ignore", category=RuntimeWarning)


class Command(BaseCommand):
    args = 'bla bla bla'

    def handle(self, *args, **options):
        games_parsed_report()
