# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

import re
import dateutil.parser


def camelcase_to_underscore(name):
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()

def remove_not_digits(str):
    non_decimal = re.compile(r'[^\d.]+')
    return non_decimal.sub('', str)


def comma_to_dot(str):
    return str.replace(',', '.')


def create_fk_obj(model, val, field='title'):
    req = {field: val}
    objects = model.objects.filter(**req)
    if objects.count() > 1: # freaking mutlithreading magic I suppose
        obj = objects[0]
        to_del = objects[1]
        to_del.basegame_set.update(**{camelcase_to_underscore(model.__name__):obj})
        to_del.delete()
        return obj
    else:
        obj, created = model.objects.get_or_create(**req)
    return obj


def remove_buttons(bs):
    for b in bs.findAll('button'):
        b.extract()
    return bs


def parse_russian_date(str):
    month_mapping = [
        ('января', 'jan'),
        ('февраля', 'feb'),
        ('марта', 'mar'),
        ('апреля', 'apr'),
        ('мая', 'may'),
        ('июня', 'jun'),
        ('июля', 'jul'),
        ('августа', 'aug'),
        ('сентября', 'sep'),
        ('октября', 'oct'),
        ('ноября', 'nov'),
        ('декабря', 'dec'),
    ]
    for m in month_mapping:
        str = str.replace(m[0], m[1])
    str = str.replace(' г.', '')
    date = dateutil.parser.parse(str).date()
    return date


def disable_autoplay(str):
    return str.replace('autoplay=1','autoplay=0')


def get_pricing(str):
    if str in ['0', 'Бесплатно', 'Бесплатные']:
        return None, None, 'Free'
    # THB99.99
    if str.startswith('THB'):
        cur = 'THB'
        price = str[3:]
    elif 'руб' in str:  # 43,50 руб.
        cur = 'р'
        price = comma_to_dot(str[:-4])
    else:
        # eg '3 600,00 $.'
        price, cur = ''.join(str.split()[:-1]), str.split()[-1]
        cur = cur.replace('.', '')
        price = comma_to_dot(price)
    return price, cur, 'Commercial'