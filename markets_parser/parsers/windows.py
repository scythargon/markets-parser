# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

import ipdb
from base import BaseParser
from helpers import comma_to_dot, remove_not_digits, remove_buttons, parse_russian_date, disable_autoplay, \
    get_pricing

"""
e.g.
http://www.windowsphone.com/ru-ru/store/app/radiant-defense/ab93968f-ce5d-403b-84aa-f36847290ec5

"""


class WindowsParser(BaseParser):

    @classmethod
    def create_game(cls):
        from markets_parser.models import WindowsGame
        return WindowsGame()

    @classmethod
    def parse_bs4(cls, bs):
        #with ipdb.launch_ipdb_on_exception():
        try:
            title=bs.find('h1', {'itemprop':'name'}).text
        except:
            pass
        try:
            description = bs.find('pre', {'itemprop':'description'}).text.replace('\n', '<br>\n')
        except:
            pass
        try:
            icon = bs.find('img', {'itemprop':'image'}).attrs['src']
        except:
            pass
        try:
            publisher = bs.findAll(lambda tag: tag.get('itemprop', '') == 'publisher')[0].text
        except:
            pass

        category = ''
        try:
            category = bs.find('strong', {'itemprop': 'applicationCategory'}).text
            if category == 'игры':
                category = bs.find('strong', {'itemprop': 'applicationSubCategory'}).text
        except:
            pass

        screenshots = bs.findAll('img', {'itemprop':'screenshot'})
        screenshots = ' '.join([s.parent.attrs['href'] for s in screenshots])
        try:
            price = bs.find('span', {'itemprop':'price'}).text
            price, currency, pricing_type = get_pricing(price)
        except:
            pass

        try:
            rating = bs.find('meta', {'itemprop':'ratingValue'}).attrs['content']
            rating = comma_to_dot(rating)
            rating = float(rating)
            rating = int(rating*10)/10.
            votes = bs.find('meta', {'itemprop':'ratingCount'}).attrs['content']
        except:
            pass
        try:
            size = bs.find('div',{'id':'packageSize'}).span.text
        except:
            pass

        try:
            age_category = bs.find('div',{'id':'softwareRating'}).a.text
        except:
            pass

        try:
            last_update_date = bs.find('meta',{'itemprop':'datePublished'}).attrs['content']
        except:
            pass
        try:
            version = bs.find('span',{'itemprop':'softwareVersion'}).text
        except:
            pass
        try:
            languages = bs.find('div',{'id':'languageList'}).find('div',{'class':'textContainer'}).text.strip().replace('\n',', ')
        except:
            pass

        try:
            platform_version_required = bs.find('div',{'id':'softwareRequirements'}).ul.text.strip()
        except:
            pass

        return locals()
