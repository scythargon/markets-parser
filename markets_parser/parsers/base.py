import requests
from bs4 import BeautifulSoup
from helpers import create_fk_obj
from markets_parser.exceptions import GeneralRequestsException, SeemsLikeWeAreBannedException
from socket import error as SocketError
import errno


class BaseParser():

    @classmethod
    def load(cls, url):  # used only for in-admin reparsing
        headers = { 'User-Agent': 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36'}
        page = requests.get(url, headers=headers)
        return page.text

    @classmethod
    def parse(cls, game=None, title=None, url=None, proxy=None, html=None, **kwargs):
        from ..models import *
        if game and not url:
            raise Exception('give me the url!')
        if not game:
            game = cls.create_game()
            game.reparse = False
        if title:
            pass
        if game.reparse:
            html = cls.load(url)
        else:
            if not html:
                raise Exception('da fakk is going on')
        bs = BeautifulSoup(html, "lxml")

        data = cls.parse_bs4(bs, **kwargs)

        data['download_url'] = url

        if not 'title' in data:
            raise SeemsLikeWeAreBannedException('gotcha!')  # that's retarded, Boromir, since bs4 parser will raise an error

        update = {}
        for k, v in {k: v for k, v in data.iteritems() if k in
                ['version', 'size', 'icon', 'description', 'whats_new', 'price', 'rating', 'votes', 'download_url',
                 'current_version_rating', 'current_version_votes', 'languages', 'screenshots', 'title', 'ipad_screenshots',
                 'price_string',
                 'platform_version_required','last_update_date', 'video', 'price', 'for_iphone', 'for_ipad']}.iteritems():
            #setattr(game, k, v)
            update[k] = v

        for k, model in [
                ('publisher', Publisher),
                ('category', Category),
                ('pricing_type', PricingType),
                ('age_category', AgeCategory),
                ('users_amount', UsersAmount),
                ('currency', Currency),
                ('pricing_type', PricingType),
            ]:
            if k in data:
                v = data[k]
                if v:
                    v = create_fk_obj(model, v)
                    #setattr(game, k, v)
                    update[k] = v
        '''
        if 'screenshots' in data:
            game.screenshots.all().delete()
            game.screenshots.clear()
            for s in data['screenshots']:
                s = create_fk_obj(Screenshot, s, 'img_url')
                game.screenshots.add(s)

        if 'languages' in data:
            game.languages.clear()
            for s in data['languages']:
                s = create_fk_obj(Language, s, 'title')
                game.languages.add(s)
        '''
        # BaseGame.objects.filter(pk=game.pk).update(**update)
        # debug for the Throne of Debugging!
        del bs
        del data
        if not game.pk:
            game.save()
        model = type(game.get_real_model())
        set = model.objects.filter(pk=game.pk)
        for k in update:
            d = {k: update[k]}
            set.update(**d)
        return game