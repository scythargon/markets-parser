import requests

from base import BaseParser
from helpers import comma_to_dot, remove_not_digits, remove_buttons, parse_russian_date, disable_autoplay, \
    get_pricing

"""
e.g.
https://play.google.com/store/apps/details?id=com.ubisoft.rabbids.bigbang.ggp
"""

class AndroidParser(BaseParser):

    @classmethod
    def load(cls, url):  # used only for in-admin reparsing
        headers = { 'User-Agent': 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36'}
        page = requests.get(url+'&hl=ru', headers=headers)
        return page.text

    @classmethod
    def create_game(cls):
        from markets_parser.models import AndroidGame
        return AndroidGame()

    @classmethod
    def parse_bs4(cls, bs4page):
        android = bs4page

        try:
            title = android.find('div', {'itemprop': 'name'}).text
        except:
            pass

        try:
            publisher = android.find('a', {'class': 'document-subtitle primary'}).text.strip()
        except:
            pass

        try:
            icon = android.find('img', {'itemprop': 'image'}).attrs['src']
        except:
            pass

        try:
            category = android.find('span', {'itemprop': 'genre'}).text
        except:
            pass

        try:
            rating = android.find('div', {'class': 'score'}).text
            rating = comma_to_dot(rating)
        except:
            pass

        try:
            price = android.find('meta', {'itemprop': 'price'}).attrs['content']
            price, currency, pricing_type = get_pricing(price)
        except:
            pass

        try:
            votes = android.find('span', {'class': 'reviews-num'}).text
            votes = remove_not_digits(votes)
        except:
            pass

        try:
            description = str(android.find('div', {'class': 'id-app-orig-desc'}))
        except:
            pass

        try:
            whats_new = str(remove_buttons(android.find('div', {'class': 'whatsnew'})))
        except:
            pass

        try:
            last_update_date = android.find('div', {'itemprop': 'datePublished'}).text
            last_update_date = parse_russian_date(last_update_date)
        except:
            pass

        try:
            size = android.find('div', {'itemprop': 'fileSize'}).text.strip()
        except:
            pass

        try:
            users_amount = android.find('div', {'itemprop': 'numDownloads'}).text.strip()
        except:
            pass

        try:
            version = android.find('div', {'itemprop': 'softwareVersion'}).text.strip()
        except:
            pass

        try:
            platform_version_required = android.find('div', {'itemprop': 'operatingSystems'}).text.strip()
        except:
            pass

        try:
            age_category = android.find('div', {'itemprop': 'contentRating'}).text.strip()
        except:
            pass

        try:
            video = android.find('span', {'class': 'details-trailer'}).find('span', {'class': 'preview-overlay-container'}).attrs['data-video-url']
            video = disable_autoplay(video)
        except:
            pass

        try:
            screenshots = android.findAll('img', {'class': 'screenshot'})
            screenshots = ' '.join([s.attrs['src'] for s in screenshots])
            screenshots = screenshots.replace('h310-rw', 'h900-rw')
        except:
            pass

        return locals()
