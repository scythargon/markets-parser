# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from StringIO import StringIO
import gzip
import socket
import dbus
from base import BaseParser
from helpers import comma_to_dot, remove_not_digits, remove_buttons, parse_russian_date, disable_autoplay, \
    get_pricing
from markets_parser.exceptions import SeemsLikeWeAreBannedException

"""
e.g.
https://itunes.apple.com/ru/app/simpsony-springfield/id498375892?mt=8
"""


def log(str, filename_end):
    filename = '/tmp/markets%s' % filename_end
    print 'logged to %s' % filename
    f = open(filename, 'w')
    f.write(str)
    f.close()

class IPhoneParser(BaseParser):

    @classmethod
    def create_game(cls):
        from markets_parser.models import IPhoneGame
        return IPhoneGame()


    @classmethod
    def parse_bs4(cls, bs, **kwargs):
        """
        category should be parsed from category list page e.g. https://itunes.apple.com/ru/genre/ios-igry/id6014?mt=8
        """
        for k, v in kwargs.items():
            exec "%s = '%s'" % (k, v)
        try:
            title = bs.find('div', {'id':'title'}).h1.text
        except:
            raise SeemsLikeWeAreBannedException()
        publisher = ' '.join(bs.find('div', {'id':'title'}).h2.text.split()[1:])
        price = bs.find('div', {'class':'price'}).text
        price, currency, pricing_type = get_pricing(price)
        icon = bs.find('div',{'id':'left-stack'}).find('img',{'class': 'artwork'}).attrs['src-swap-high-dpi']

        last_update_date = parse_russian_date(bs.find('li', {'class':'release-date'}).text.split()[-1])

        version = bs.find('span', text='Версия: ').next_sibling

        size = bs.find('span', text='Размер: ').next_sibling

        try:
            languages = bs.find('span', text='Языки: ').next_sibling
        except:
            try:
                languages = bs.find('span', text='Язык: ').next_sibling
            except:
                pass
            pass

        age_category = bs.find('div', {'class':'app-rating'}).a.text

        try:
            compatibility = bs.find('span', {'class':'app-requirements'}).next_sibling
            #for_iphone, for_ipad = bool(compatibility.count('iPhone')), bool(compatibility.count('iPad'))
            del compatibility
        except:
            pass

        try:
            full_stars = len(bs.find('div',{'id':'left-stack'}).find('div', text='Текущая версия:').next_sibling.next_sibling.
                findAll(lambda tag: 'rating-star' in tag.get('class', []) and 'half' not in tag.get('class',[])))
            half_stars = len(bs.find('div',{'id':'left-stack'}).find('div', text='Текущая версия:').next_sibling.next_sibling.
                findAll(lambda tag: 'rating-star' in tag.get('class', []) and 'half' in tag.get('class',[])))

            current_version_rating = full_stars + half_stars * 0.5
            current_version_votes = bs.find('div',{'id':'left-stack'}).find('div', text='Текущая версия:').next_sibling.next_sibling.find('span', {'class':'rating-count'}).text.split()[-1]
        except:
            pass

        try:
            full_stars = len(bs.find('div',{'id':'left-stack'}).find('div', text='Все версии:').next_sibling.next_sibling.
                findAll(lambda tag: 'rating-star' in tag.get('class', []) and 'half' not in tag.get('class',[])))
            half_stars = len(bs.find('div',{'id':'left-stack'}).find('div', text='Все версии:').next_sibling.next_sibling.
                findAll(lambda tag: 'rating-star' in tag.get('class', []) and 'half' in tag.get('class',[])))

            rating = full_stars + half_stars * 0.5
            votes = bs.find('div',{'id':'left-stack'}).find('div', text='Все версии:').next_sibling.next_sibling.find('span', {'class':'rating-count'}).text.split()[-1]
        except:
            pass
        #del full_stars, half_stars

        description = str(bs.findAll('div',{'class': 'product-review'})[0].find('p'))
        if len(bs.findAll('div',{'class': 'product-review'})) > 1:
            whats_new = str(bs.findAll('div',{'class': 'product-review'})[1].find('p'))
        else:
            whats_new = None
        screenshots = ''
        try:
            screenshots = bs.findAll('div',{'class':'iphone-screen-shots'})[0].findAll('img')
            screenshots = ' '.join([s.attrs['src'] for s in screenshots])
        except:
            pass
        ipad_screenshots = ''
        try:
            ipad_screenshots = bs.findAll('div',{'class':'ipad-screen-shots'})[0].findAll('img')
            ipad_screenshots = ' '.join([s.attrs['src'] for s in ipad_screenshots])
        except:
            pass

        for_iphone = bool(screenshots)
        for_ipad = bool(ipad_screenshots)

        return locals()
