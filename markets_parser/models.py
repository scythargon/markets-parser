from django.db import models
from django.db.models.signals import post_save
from django.dispatch.dispatcher import receiver

from .parsers import AndroidParser, IPhoneParser, WindowsParser


class Publisher(models.Model):
    title = models.CharField(max_length=225)

    def __unicode__(self):
        return self.title


class Category(models.Model):
    title = models.CharField(max_length=225)

    def __unicode__(self):
        return self.title


class PricingType(models.Model):
    title = models.CharField(max_length=225)

    def __unicode__(self):
        return self.title


class Language(models.Model):
    title = models.CharField(max_length=225)

    def __unicode__(self):
        return self.title


class AgeCategory(models.Model):
    title = models.CharField(max_length=225)

    def __unicode__(self):
        return self.title


class UsersAmount(models.Model):
    title = models.CharField(max_length=225)

    def __unicode__(self):
        return self.title


class Screenshot(models.Model):
    img_url = models.TextField()

    def __unicode__(self):
        return 'Screenshot %s' % self.pk


class Video(models.Model):
    url = models.TextField()

    def __unicode__(self):
        return 'Video %s' % self.pk


class Currency(models.Model):
    title = models.CharField(max_length=225)

    def __unicode__(self):
        return self.title


class BaseGame(models.Model):
    title = models.CharField(max_length=225, null=True, blank=True)
    download_url = models.TextField()
    reparse = models.BooleanField(default=False)
    version = models.CharField(max_length=225, null=True, blank=True)
    size = models.CharField(max_length=225, null=True, blank=True)
    icon = models.TextField(null=True, blank=True)
    platform_version_required = models.TextField(null=True, blank=True)

    description = models.TextField(null=True, blank=True)
    whats_new = models.TextField(null=True, blank=True)

    price = models.FloatField(null=True, blank=True)

    rating = models.FloatField(null=True, blank=True)
    votes = models.IntegerField(null=True, blank=True)
    current_version_rating = models.FloatField(null=True, blank=True)
    current_version_votes = models.IntegerField(null=True, blank=True)

    last_update_date = models.DateTimeField(null=True, blank=True)

    last_check_date = models.DateTimeField(auto_now_add=True, auto_now=True, null=True, blank=True)

    publisher = models.ForeignKey(Publisher, null=True, blank=True)
    category = models.ForeignKey(Category, null=True, blank=True)
    pricing_type = models.ForeignKey(PricingType, null=True, blank=True)
    age_category = models.ForeignKey(AgeCategory, null=True, blank=True)
    users_amount = models.ForeignKey(UsersAmount, null=True, blank=True)
    currency = models.ForeignKey(Currency, null=True, blank=True)

    languages = models.TextField(null=True, blank=True)
    screenshots = models.TextField(null=True, blank=True)
    video = models.TextField(null=True, blank=True)
    #videos = models.ManyToManyField(Video, null=True, blank=True)
    game = models.OneToOneField('Game', null=True, blank=True)


    def __unicode__(self):
        return '%s' % self.title


    def get_real_model(self):
        if not hasattr(self, '_real_model'):
            if self.__class__ == BaseGame:
                test_obj = BaseGame.objects.filter(pk=self.pk).select_related(*[cs.__name__.lower() for cs in self.__class__.__subclasses__()]).get()
                for cs in self.__class__.__subclasses__():
                    try:
                        self._real_model = getattr(test_obj, cs.__name__.lower())
                        return self._real_model
                    except self.DoesNotExist:
                        pass
            return self
        return self._real_model


class Game(models.Model):
    title = models.CharField(max_length=225)

    def save(self, *args, **kwargs):
        new = False
        if self.pk is None:
            new = True
        super(Game, self).save(*args, **kwargs)
        if new:
            androidgame = AndroidGame(**AndroidParser.parse(title=self.cleaned_data['title']))
            self.androidgame = androidgame
            self.androidgame.save()



class AndroidGame(BaseGame):
    pass


class IPhoneGame(BaseGame):
    for_iphone = models.BooleanField(default=False)
    for_ipad= models.BooleanField(default=False)
    ipad_screenshots = models.TextField(null=True, blank=True)

    class Meta:
        verbose_name = 'iPhone game'
        verbose_name_plural = 'iPhone games'


class WindowsGame(BaseGame):
    pass


@receiver(post_save, sender=AndroidGame)
def parse_android(sender, instance, **kwargs):
    if instance.reparse:
        AndroidParser.parse(instance, url=instance.download_url)


@receiver(post_save, sender=IPhoneGame)
def parse_iphone(sender, instance, **kwargs):
    if instance.reparse:
        IPhoneParser.parse(instance, url=instance.download_url)


@receiver(post_save, sender=WindowsGame)
def parse_windows(sender, instance, **kwargs):
    if instance.reparse:
        WindowsParser.parse(instance, url=instance.download_url)