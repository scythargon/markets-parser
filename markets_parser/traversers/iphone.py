# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

import random
import time
import logging
from bs4 import BeautifulSoup
from app_metrics.utils import gauge

from django.conf import settings

from markets_parser.parsers import IPhoneParser
from markets_parser.exceptions import GeneralRequestsException, SeemsLikeWeAreBannedException
from markets_parser.tasks import parse_url
from markets_parser.models import BaseGame
from markets_parser.helpers import download_via_socket, log_to_file
from proxies.models import Proxy

from .base import BaseTraverser
from .helpers import get_hrefs


class IPhonesTraverser(BaseTraverser):

    PLATFORM_ID = 'IPhone'

    @classmethod
    def category_page_first_level_traverse(cls, url, proxy_pk=None, category=''):
        """
        https://itunes.apple.com/ru/genre/ios-igry-nastol-nye/id7004?mt=8
        """
        gauge('iphone-category-pages-first-type-parsed', 1)
        logging.info('one more iphone first type category page: %s' % url)
        text, proxy = cls.load(url, proxy_pk)
        print 'successfully loaded'
        log_to_file('first_attempt', text)
        bs = BeautifulSoup(text, 'lxml')

        sub_categories = get_hrefs( bs.find('ul', {'class': 'list alpha'}).findAll('a') )
        logging.info('iphone second level categories: %s' % len(sub_categories))

        for s in sub_categories:
            parse_url.delay(s, cls.PLATFORM_ID, cls.CATEGORY_PAGE_SECOND_LEVEL, proxy_pk=proxy_pk, category=category)

    @classmethod
    def category_page_second_level_traverse(cls, url, proxy_pk=None, category=''):
        """
        https://itunes.apple.com/ru/genre/ios-igry-nastol-nye/id7004?mt=8&letter=A
        """
        gauge('iphone-category-pages-second-type-parsed', 1)
        logging.info('one more iphone second type category page: %s' % url)
        text, proxy = cls.load(url, proxy_pk)
        print 'successfully loaded'
        bs = BeautifulSoup(text, 'lxml')

        next = bs.find('a', {'class': 'paginate-more'})

        games_div = bs.find('div', {'id': 'selectedcontent'})
        games = []
        if games_div:
            for column in games_div.findAll('div', {'class': 'column'}):
                games = get_hrefs(column.findAll('a'))
        logging.info('iphone apps: %s' % len(games))
        for g in games:
            if not BaseGame.objects.filter(download_url=g):
                parse_url.delay(g, cls.PLATFORM_ID, cls.APP_PAGE, proxy_pk=proxy_pk, category=category)
        if next:
            logging.info('iphone: found next page')
            parse_url.delay(next.attrs['href'], cls.PLATFORM_ID, cls.CATEGORY_PAGE_SECOND_LEVEL, proxy_pk=proxy_pk, category=category)


    @classmethod
    def app_page_traverse(cls, url, proxy_pk, category=''):
        logging.info('parsing a new game')
        #time.sleep(random.randint(10,20))
        if BaseGame.objects.filter(download_url=url):
            logging.info('game is already parsed')
            return
        proxy = None
        proxy_num = 0
        while True:
            try:
                html, proxy = cls.load(url, proxy_pk)
                game = IPhoneParser.parse(url=url, html=html, category=category)
                break
            except SeemsLikeWeAreBannedException:
                proxy.deactivate_for_platform(cls.PLATFORM_ID)
                proxy = cls.get_proxy()
                proxy_num += 1
            logging.info('app loading attempt #%s %s:%s(pk=%s) for %s' % (proxy_num, proxy.ip, proxy.port, proxy.port, url) )

        print 'game is parsed: http://%s/admin/markets_parser/iphonegame/%s/' % (settings.RUNNING_ADDRESS, game.pk)

    CATEGORY_PAGE_FIRST_LEVEL = 'category_page_first_level'
    CATEGORY_PAGE_SECOND_LEVEL = 'category_page_second_level'
    APP_PAGE = 'app_page'