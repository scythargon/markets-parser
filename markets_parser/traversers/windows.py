# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

import random
import time
import logging
from bs4 import BeautifulSoup
from app_metrics.utils import gauge

from django.conf import settings

from markets_parser.parsers import WindowsParser
from markets_parser.exceptions import GeneralRequestsException, SeemsLikeWeAreBannedException
from markets_parser.tasks import parse_url
from markets_parser.models import BaseGame
from proxies.models import Proxy

from .base import BaseTraverser
from .helpers import get_hrefs


class WindowsTraverser(BaseTraverser):

    PLATFORM_ID = 'Windows'

    @classmethod
    def category_page_traverse(cls, url, proxy_pk=None, _subcategories=False, _next_page=False):
        """
        e.g. http://www.windowsphone.com/ru-ru/store/new-apps
        """
        gauge('windows-category-pages-parsed', 1)
        logging.info('one more category page: %s' % url)
        text, proxy = cls.load(url, proxy_pk)
        print 'successfully loaded'
        bs = BeautifulSoup(text, 'lxml')

        sub_categories = []
        if _subcategories:
            sub_categories = get_hrefs( bs.findAll("a", {"data-ov" : lambda L: L and L.startswith('GameLeftMerch')}) )
            if not sub_categories:
                sub_categories = get_hrefs( bs.findAll("a", {"data-ov" : lambda L: L and L.startswith('AppLeftMerch')}) )
            logging.info('subcategories: %s' % len(sub_categories))

        apps = []
        apps = get_hrefs( bs.findAll("a", {"data-os": "app"}) )
        apps = apps[0:-1:2]

        next_page = None

        if _next_page:
            next_page = bs.find("a", {"data-os": "storeSearchPagination"}, text='далее >')
            if next_page:
                next_page = next_page.attrs['href']
                next_page = 'http://www.windowsphone.com%s' % next_page

        for s in sub_categories:
            parse_url.delay(s, cls.PLATFORM_ID, cls.CATEGORY_PAGE, proxy_pk=None, _subcategories=False, _next_page=True)
        if next_page:
            #sleep_time = random.randint(10,20)
            #logging.info('sleeping before next_page: %ss' % sleep_time)
            #time.sleep(sleep_time)
            parse_url.delay(next_page, cls.PLATFORM_ID, cls.CATEGORY_PAGE, proxy_pk=proxy.pk, _subcategories=False, _next_page=True)

        new = 0
        logging.info('total apps: %s' % len(apps))
        for app in apps:
            if BaseGame.objects.filter(download_url=app):
                continue
            else:
                parse_url.delay(app, cls.PLATFORM_ID, cls.APP_PAGE, proxy_pk=proxy.pk)
                new += 1
        logging.info('new apps: %s' % new)

    @classmethod
    def app_page_traverse(cls, url, proxy_pk):
        logging.info('parsing a new game')
        #time.sleep(random.randint(10,20))
        if BaseGame.objects.filter(download_url=url):
            logging.info('game is already parsed')
            return
        proxy = None
        proxy_num = 0
        while True:
            try:
                html, proxy = cls.load(url, proxy_pk)
                game = WindowsParser.parse(url=url, html=html)
                break
            except SeemsLikeWeAreBannedException:
                proxy.deactivate_for_platform(cls.PLATFORM_ID)
                proxy = cls.get_proxy()
                proxy_num += 1
            logging.info('app loading attempt #%s %s:%s(pk=%s) for %s' % (proxy_num, proxy.ip, proxy.port, proxy.port, url) )

        print 'game is parsed: http://%s/admin/markets_parser/windowsgame/%s/' % (settings.RUNNING_ADDRESS, game.pk)


    CATEGORY_PAGE = 'category_page'
    APP_PAGE = 'app_page'