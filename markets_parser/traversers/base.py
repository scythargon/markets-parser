import requests
from proxies.models import Proxy
from socket import error as SocketError
import errno
import logging


class BaseTraverser():

    PLATFORM_ID = None

    PAGE_TYPES = ()

    # some inner traversing methods

    @classmethod
    def load(cls, url, proxy_pk=-1):
        headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36'}

        if proxy_pk >= 0:
            proxy = Proxy.objects.get(pk=proxy_pk)
        else:
            proxy = cls.get_proxy()
        proxy_num = 0
        while True:
            try_num = 1
            while try_num < 4:
                try:
                    proxy.use_by_platform(cls.PLATFORM_ID)
                    r = requests.get(url, headers=headers, timeout=10*try_num, proxies={'http': 'http://%s:%s' % (proxy.ip, proxy.port)})
                    if r.status_code != 200:
                        logging.info('wrong response code: %s' % r.status_code)
                        try_num += 1
                        continue
                    return (r.text, proxy)
                except (requests.exceptions.ConnectTimeout, requests.exceptions.ReadTimeout, requests.exceptions.ConnectionError):
                    try_num += 1
                except SocketError as e:
                    if e.errno != errno.ECONNRESET:
                        raise e # Not error we are looking for
                    try_num += 1
            proxy.deactivate_for_platform(cls.PLATFORM_ID)
            proxy = cls.get_proxy()
            proxy_num += 1
            logging.info('loading attempt #%s %s:%s(pk=%s) for %s' % (proxy_num, proxy.ip, proxy.port, proxy.pk, url) )

    @classmethod
    def traverse(cls, page_type, url, proxy_pk, **kwargs):
        getattr(cls, '%s_traverse' % page_type)(url, proxy_pk, **kwargs)

    @classmethod
    def get_proxy(cls):
        proxy = Proxy.quick_give_me_working_one(platform=cls.PLATFORM_ID)
        return proxy
