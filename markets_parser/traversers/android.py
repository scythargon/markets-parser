# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

import random
import time
import logging
from bs4 import BeautifulSoup
from app_metrics.utils import gauge
from urllib import urlencode

from django.conf import settings

from markets_parser.parsers import AndroidParser
from markets_parser.exceptions import GeneralRequestsException, SeemsLikeWeAreBannedException
from markets_parser.tasks import parse_url
from markets_parser.models import BaseGame
from markets_parser.helpers import download_via_socket, log_to_file
from proxies.models import Proxy

from .base import BaseTraverser
from .helpers import get_hrefs


class AndroidTraverser(BaseTraverser):

    PLATFORM_ID = 'Android'

    @classmethod
    def category_page_traverse(cls, url, proxy_pk=None, start=0, num=24):
        """
        https://play.google.com/store/apps/category/GAME_CASINO/collection/topselling_free
        """
        gauge('android-category-pages-parsed', 1)
        logging.info('one more android category page: %s' % url+'?'+urlencode({'start': start, 'num': num}))
        text, proxy = cls.load(url+'?'+urlencode({'start': start, 'num': num}), proxy_pk)
        print 'successfully loaded'
        bs = BeautifulSoup(text, 'lxml')

        games = []  # $('div[class="details"] a[class=title]')
        for div in bs.findAll('div', {'class': 'details'}):
            games.append(div.find('a', {'class': 'title'}).attrs['href'])
        logging.info('android apps: %s' % len(games))

        if len(games) == num:
            logging.info('android: need to load next page: %s, start: %s num: %s' % (url, start, num))
            parse_url.delay(url, cls.PLATFORM_ID, cls.CATEGORY_PAGE, proxy_pk=proxy_pk, start=start+num, num=num)
        else:
            logging.info('android: got %s pages instead of %s, stopping categories parsing' % (len(games), num))


        for g in games:
            game_url = 'https://play.google.com'+g+'&hl=ru'
            if not BaseGame.objects.filter(download_url=game_url):
                parse_url.delay(game_url, cls.PLATFORM_ID, cls.APP_PAGE, proxy_pk=proxy_pk)


    @classmethod
    def app_page_traverse(cls, url, proxy_pk):
        logging.info('parsing a new game')
        #time.sleep(random.randint(10,20))
        if BaseGame.objects.filter(download_url=url):
            logging.info('game is already parsed')
            return
        proxy = None
        proxy_num = 0
        while True:
            try:
                html, proxy = cls.load(url, proxy_pk)
                game = AndroidParser.parse(url=url, html=html)
                break
            except SeemsLikeWeAreBannedException:
                proxy.deactivate_for_platform(cls.PLATFORM_ID)
                proxy = cls.get_proxy()
                proxy_num += 1
            logging.info('app loading attempt #%s %s:%s(pk=%s) for %s' % (proxy_num, proxy.ip, proxy.port, proxy.port, url) )

        print 'game is parsed: http://%s/admin/markets_parser/androidgame/%s/' % (settings.RUNNING_ADDRESS, game.pk)

    CATEGORY_PAGE = 'category_page'
    APP_PAGE = 'app_page'