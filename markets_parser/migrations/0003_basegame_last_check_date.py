# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('markets_parser', '0002_iphonegame_ipad_screenshots'),
    ]

    operations = [
        migrations.AddField(
            model_name='basegame',
            name='last_check_date',
            field=models.DateTimeField(auto_now=True, auto_now_add=True, null=True),
            preserve_default=True,
        ),
    ]
