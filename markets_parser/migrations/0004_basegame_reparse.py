# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('markets_parser', '0003_basegame_last_check_date'),
    ]

    operations = [
        migrations.AddField(
            model_name='basegame',
            name='reparse',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
