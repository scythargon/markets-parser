# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AgeCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=225)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='BaseGame',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=225, null=True, blank=True)),
                ('download_url', models.TextField()),
                ('version', models.CharField(max_length=225, null=True, blank=True)),
                ('size', models.CharField(max_length=225, null=True, blank=True)),
                ('icon', models.TextField(null=True, blank=True)),
                ('platform_version_required', models.TextField(null=True, blank=True)),
                ('description', models.TextField(null=True, blank=True)),
                ('whats_new', models.TextField(null=True, blank=True)),
                ('price', models.FloatField(null=True, blank=True)),
                ('rating', models.FloatField(null=True, blank=True)),
                ('votes', models.IntegerField(null=True, blank=True)),
                ('current_version_rating', models.FloatField(null=True, blank=True)),
                ('current_version_votes', models.IntegerField(null=True, blank=True)),
                ('last_update_date', models.DateTimeField(null=True, blank=True)),
                ('languages', models.TextField(null=True, blank=True)),
                ('screenshots', models.TextField(null=True, blank=True)),
                ('video', models.TextField(null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='AndroidGame',
            fields=[
                ('basegame_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='markets_parser.BaseGame')),
            ],
            options={
            },
            bases=('markets_parser.basegame',),
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=225)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Currency',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=225)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Game',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=225)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='IPhoneGame',
            fields=[
                ('basegame_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='markets_parser.BaseGame')),
                ('for_iphone', models.BooleanField(default=False)),
                ('for_ipad', models.BooleanField(default=False)),
            ],
            options={
                'verbose_name': 'iPhone game',
                'verbose_name_plural': 'iPhone games',
            },
            bases=('markets_parser.basegame',),
        ),
        migrations.CreateModel(
            name='Language',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=225)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PricingType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=225)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Publisher',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=225)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Screenshot',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('img_url', models.TextField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UsersAmount',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=225)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Video',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('url', models.TextField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='WindowsGame',
            fields=[
                ('basegame_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='markets_parser.BaseGame')),
            ],
            options={
            },
            bases=('markets_parser.basegame',),
        ),
        migrations.AddField(
            model_name='basegame',
            name='age_category',
            field=models.ForeignKey(blank=True, to='markets_parser.AgeCategory', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='basegame',
            name='category',
            field=models.ForeignKey(blank=True, to='markets_parser.Category', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='basegame',
            name='currency',
            field=models.ForeignKey(blank=True, to='markets_parser.Currency', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='basegame',
            name='game',
            field=models.OneToOneField(null=True, blank=True, to='markets_parser.Game'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='basegame',
            name='pricing_type',
            field=models.ForeignKey(blank=True, to='markets_parser.PricingType', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='basegame',
            name='publisher',
            field=models.ForeignKey(blank=True, to='markets_parser.Publisher', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='basegame',
            name='users_amount',
            field=models.ForeignKey(blank=True, to='markets_parser.UsersAmount', null=True),
            preserve_default=True,
        ),
    ]
