

class SeemsLikeWeAreBannedException(Exception):
    pass


class GeneralRequestsException(Exception):
    pass